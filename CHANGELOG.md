# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0]
### Added
	- Better yaml file example

### Update
	- Readme
	- Contributing

## [0.4.0]
### Added
	- Project creation for groups
	- Clean up

## [0.3.0]
### Added
	- Will now create Gitlab project by reading from "install.yaml" file.

## [0.2.0]
### Added
	- Module for accessing Gitlab api

## [0.1.0]
### Added
	- Endpoints

## [0.0.2]
### Added
	- CONTRIBUTING.md
	- CODE OF CONDUCT.MD
	- LICENSE
	- CHANGELOG.md
	- .gitignore
	- Issue templates
	- setup.py
	- empty test

### Updated
	- README.md

[Unreleased]: https://gitlab.com/justinekizhak/create_project/compare/v0.0.2...develop
[0.0.2]: https://gitlab.com/justinekizhak/create_project/compare/v0.0.1...v0.0.2
[0.1.0]: https://gitlab.com/justinekizhak/create_project/compare/v0.0.2...v0.1.0
[0.2.0]: https://gitlab.com/justinekizhak/create_project/compare/v0.1.0...v0.2.0
[0.3.0]: https://gitlab.com/justinekizhak/create_project/compare/v0.2.0...v0.3.0
[0.4.0]: https://gitlab.com/justinekizhak/create_project/compare/v0.3.0...v0.4.0
[0.5.0]: https://gitlab.com/justinekizhak/create_project/compare/v0.4.0...v0.5.0
